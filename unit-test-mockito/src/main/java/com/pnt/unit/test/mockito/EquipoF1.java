package com.pnt.unit.test.mockito;

public class EquipoF1 {

    private final Piloto piloto1;
    private final Piloto piloto2;
    private final String nombreEquipo;

    public EquipoF1(Piloto piloto1, Piloto piloto2, String nombreEquipo) {
        this.piloto1 = piloto1;
        this.piloto2 = piloto2;
        this.nombreEquipo = nombreEquipo;
    }

    /**
     * El equipo quiere verificar que tan saludable están sus pilotos. Para ello
     * dispone del método verificarEstadoSaludEquipo. Pista: "Mock"
     *
     * @return Si el promedio del nivel de energía de los pilotos es superior a
     * 30 nos retorna Equipo: [NOMBRE_EQUIPO] Reporte: El estado fisico de los
     * pilotos es saludable. Si el promedio es menor o igual a 30, retorna
     * Equipo: [NOMBRE_EQUIPO] Reporte: Los pilotos estan yendo muy seguido a
     * Wendy´s
     */
    public String verificarEstadoSaludEquipo() {
        int energiaTotal = (piloto1.getEnergia() + piloto2.getEnergia()) / 2;
        if (energiaTotal > 30) {
//            return "Equipo: " + this.nombreEquipo + " Reporte: El estado fisico de los pilotos es saludable";
        }
//        return "Equipo: " + this.nombreEquipo + " Reporte: Los pilotos estan yendo muy seguido a Wendy´s";
        return "";
    }

    /**
     * En cada entrenamiento, el piloto principal de la escuderia entrena 2
     * veces, y el secundario entrena 1 vez.
     *
     * @param horasAEntrenar horas que van a entrenar cada uno de los pilotos.
     *
     * @return Si el entrenamiento finaliza correctamente retorna true. Si
     * alguno de los pilotos está cansado no entrenará, en ese caso, retornar
     * false
     */
    public boolean entrenarIntegrantes(int horasAEntrenar) {
        return false;
    }

    /**
     * El equipo de Formula 1 quiere saber si superó su récord de velocidad en
     * la temporada. Su marca anterior era de 290 km/h. Consulta al ingeniero la
     * velocidad máxima del equipo, y este les calcula su velocidad máxima.
     *
     * @return Si la velocidad que informa el ingeniero supera al record,
     * retornar true, de lo contrario, retornar false
     */
    public boolean batimosRecordDeVelocidad() {
        return false;
    }
}
