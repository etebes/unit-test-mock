/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pnt.unit.test.mockito;

import java.util.Random;

/**
 *
 * @author usuario
 */
public class Piloto {

    private String nombre;
    private int energia;
    private final double velocidadMaxima;

    public double getVelocidadMaxima() {
        return velocidadMaxima;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getEnergia() {
        return energia;
    }

    public Piloto(String nombre) {
        this.nombre = nombre;
        Random rn = new Random();
        this.energia = rn.nextInt(100 - 0 + 1);
        this.velocidadMaxima = rn.nextDouble() * 300;
    }

    public void entrenar(int horasAEntrenar) throws CansancioException {
        this.energia = this.energia - horasAEntrenar * 20;
        if (getEnergia() < 0) {
            throw new CansancioException();
        }
    }

}
