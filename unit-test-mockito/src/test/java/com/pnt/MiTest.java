package com.pnt;

import com.pnt.unit.test.mockito.CansancioException;
import com.pnt.unit.test.mockito.EquipoF1;
import com.pnt.unit.test.mockito.Piloto;
import junit.framework.Assert;
import static junit.framework.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import org.junit.Before;
import org.junit.Test;

public class MiTest {

    private EquipoF1 equipoFormula1;

    Piloto pilotoMockeado;

    Piloto piloto1 = new Piloto("Michael Schumacher");

    Piloto piloto2 = new Piloto("Rubens Barrichello");

    @Before
    public void setUp() {
    }

    @Test
    public void verificarEstadoSaludEquipo_conPromedioDeSaludMayorALaCota_retornaEstadoOk() {

        equipoFormula1 = new EquipoF1(pilotoMockeado, pilotoMockeado, "Ferrari");
        assertEquals("Equipo: Ferrari Reporte: El estado fisico de los pilotos es saludable", equipoFormula1.verificarEstadoSaludEquipo());

    }

    @Test
    public void verificarEstadoSaludEquipo_conPromedioDeSaludMenorALaCota_retornaEstadoOk() {

        equipoFormula1 = new EquipoF1(pilotoMockeado, pilotoMockeado, "Ferrari");
        assertEquals("Equipo: Ferrari Reporte: Los pilotos estan yendo muy seguido a Wendy´s", equipoFormula1.verificarEstadoSaludEquipo());

    }

    @Test
    public void entrenarIntegrantes_conIntegrantesConEnergiaSuficientes_retornaOK() throws CansancioException {
        //queremos verificar la cantidad de veces que se llamo el metodo entrenar para cada piloto

        equipoFormula1 = new EquipoF1(piloto1, piloto2, "Ferrari");

        assertEquals("Michael Schumacher", piloto1.getNombre());
        assertEquals("Rubens Barrichello", piloto2.getNombre());

        assertTrue(equipoFormula1.entrenarIntegrantes(10));

    }

    @Test
    public void entrenarIntegrantes_conIntegrantesConEnergiaInsuficientes_retornaExcepcion() throws CansancioException {
        //queremos verificar la cantidad de veces que se llamo el metodo entrenar para cada piloto

        equipoFormula1 = new EquipoF1(piloto1, piloto2, "Ferrari");

        assertEquals("Michael Schumacher", piloto1.getNombre());
        assertEquals("Rubens Barrichello", piloto2.getNombre());

        assertFalse(equipoFormula1.entrenarIntegrantes(10));
    }

    @Test
    public void batimosRecordDeVelocidad_conVelocidadSuficiente_retornaOk() {
        //queremos testear un metodo que invoca a un metodo estatico.
        Piloto piloto3 = new Piloto("Michael Schumacher");
        Piloto piloto4 = new Piloto("Rubens Barrichello");
        equipoFormula1 = new EquipoF1(piloto3, piloto4, "Ferrari");

        Assert.assertTrue(equipoFormula1.batimosRecordDeVelocidad());

    }

}
