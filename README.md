##Objetivos del taller:
* Aprender el uso de mockito y powermockito, para el testing unitario. Para ello realizaremos el testing unitario de una clase que tiene dependencias de otras que tienen un comportamiento aleatorio.

##Consignas:
* Editar solamente las clases MiTest y EquipoF1.
* Crear los test unitarios que verifiquen los comportamientos de los métodos.
* Sugerencia: hacer TDD.

1. Los equipos de formula 1 tienen 2 pilotos. Los pilotos conocen su propio nivel de energia. El equipo quiere verificar que tan saludable están sus pilotos. Para ello dispone del método **verificarEstadoSaludEquipo**. Si el promedio del nivel de energía de los pilotos es superior a 30 nos retorna *Equipo: [NOMBRE_EQUIPO] Reporte: El estado fisico de los pilotos es saludable*. Pista: "Mock"

2. Para el método anterior, Si el promedio es <= 30, retorna *Equipo: [NOMBRE_EQUIPO] Reporte: Los pilotos estan yendo muy seguido a Wendy´s*

3. El equipo de Formula 1 entrena a sus pilotos. Para ello expone el método **entrenarIntegrantes** En cada entrenamiento, el piloto principal de la escuderia entrena 2 veces, y el secundario entrena 1 vez. Si el entrenamiento finaliza correctamente el método de la escudería retorna true. Si alguno de los pilotos está cansado no entrenará, en ese caso, retornar false. Pista: "Spy"

4. El equipo de Formula 1 quiere saber si superó su récord de velocidad en la temporada. Su marca anterior era de 290 km/h. Para eso expone el método **batimosRecordDeVelocidad**. Dicho método consulta al ingeniero la velocidad máxima del equipo, y este les calcula su velocidad máxima. Si esta supera al record, retornar true, de lo contrario, retornar false. Pista: "método estático mock"